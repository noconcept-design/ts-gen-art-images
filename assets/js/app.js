"use strict";
// util
function GetRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}
function GetRandomInt(min, max) {
    return Math.floor(GetRandomFloat(min, max));
}
function FromPolar(v, theta) {
    return [v * Math.cos(theta), v * Math.sin(theta)];
}
function ToLuma(r, g, b) {
    return 0.2126 * r + 0.7152 * g + 0.0722 * b;
}
function Clamp(min, max, value) {
    return value > max ? max : (value < min ? min : value);
}
var MaxParticleSize = 5;
var Particle = /** @class */ (function () {
    function Particle(w, h, palette) {
        this.w = w;
        this.h = h;
        this.palette = palette;
        this.x = 0;
        this.y = 0;
        this.speed = 0;
        this.theta = 0;
        this.radius = 1.0;
        this.ttl = 500;
        this.lifetime = 500;
        this.alpha = 0.8;
        this.color = 'black';
        this.reset();
    }
    Particle.prototype.reset = function () {
        this.x = GetRandomFloat(0, this.w);
        this.y = GetRandomFloat(0, this.h);
        this.speed = GetRandomFloat(0, 3.0);
        this.theta = GetRandomFloat(0, Math.PI * 2);
        this.radius = GetRandomFloat(0.05, 1.0);
        this.lifetime = this.ttl = GetRandomInt(25, 50);
        this.color = this.palette[GetRandomInt(0, this.palette.length)];
        this.ttl = this.lifetime = GetRandomInt(25, 50);
    };
    Particle.prototype.imageComplementLuma = function (imageData) {
        var p = Math.floor(this.x) + Math.floor(this.y) * imageData.width;
        // image rgba
        var i = Math.floor(p * 4);
        var r = imageData.data[i + 0];
        var g = imageData.data[i + 1];
        var b = imageData.data[i + 2];
        // const a = imageData.data[i + 3];
        var luma = ToLuma(r, g, b);
        var ln = 1 - luma / 255.0;
        return ln;
    };
    Particle.prototype.Update = function (imageData) {
        var ln = this.imageComplementLuma(imageData);
        var lt = (this.lifetime - this.ttl) / this.lifetime;
        this.alpha = lt;
        var dRadius = GetRandomFloat(-MaxParticleSize / 5, MaxParticleSize / 5);
        var dSpeed = GetRandomFloat(-0.21, 0.21);
        var dTheta = GetRandomFloat(-Math.PI / 8, Math.PI / 8);
        // compute values
        this.speed += dSpeed;
        this.theta += dTheta;
        var _a = FromPolar(this.speed, this.theta * ln), dx = _a[0], dy = _a[1];
        this.x += dx;
        this.y += dy;
        this.x = Clamp(0, this.w, this.x);
        this.y = Clamp(0, this.h, this.y);
        this.radius += dRadius;
        this.radius = Clamp(0, MaxParticleSize, this.radius) * ln;
        // lifetime
        this.ttl += -1;
        if (this.ttl === 0) {
            this.reset();
        }
    };
    Particle.prototype.Draw = function (ctx) {
        ctx.save();
        this.experiment2(ctx);
        ctx.restore();
    };
    Particle.prototype.experiment2 = function (ctx) {
        ctx.fillStyle = this.color;
        ctx.globalAlpha = this.alpha;
        var circle = new Path2D();
        circle.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fill(circle);
    };
    return Particle;
}());
var ParticleCount = 500;
var ColorPalettes = [
    ['#cdb4db', '#ffc8dd', '#ffafcc', '#bde0fe', '#a2d2ff'],
    ['#ef476f', '#ffd166', '#06d6a0', '#118ab2', '#073b4c'],
    ['#3e5641', '#a24936', '#d36135', '#282b28', '#83bca9'],
    ['#c9e4ca', '#87bba2', '#55828b', '#3b6064', '#364958'],
    ['#f8f4e3', '#d4cdc3', '#d5d0cd', '#a2a392', '#9a998c'],
    ['#2c363f', '#e75a7c', '#f2f5ea', '#d6dbd2', '#bbc7a4'],
    ['#880d1e', '#dd2d4a', '#f26a8d', '#f49cbb', '#cbeef3'],
    ['#3e442b', '#6a7062', '#8d909b', '#aaadc4', '#d6eeff'],
    ['#e63946', '#f1faee', '#a8dadc', '#457b9d', '#1d3557'],
];
var Simulation = /** @class */ (function () {
    function Simulation(width, height) {
        this.width = width;
        this.height = height;
        this.particles = [];
        this.palette = [];
        this.init = false;
        this.palette = ColorPalettes[GetRandomInt(0, ColorPalettes.length)];
        for (var i = 0; i < ParticleCount; i++) {
            this.particles.push(new Particle(this.width, this.height, this.palette));
        }
    }
    Simulation.prototype.Update = function (imageData) {
        this.particles.forEach(function (p) { return p.Update(imageData); });
    };
    Simulation.prototype.Draw = function (ctx) {
        if (!this.init) {
            ctx.fillStyle = this.palette[0];
            ctx.fillRect(0, 0, this.width, this.height);
            this.init = true;
        }
        this.particles.forEach(function (p) { return p.Draw(ctx); });
    };
    return Simulation;
}());
function createDrawCanvas(imageCtx, width, height) {
    var updateFrameRate = 50;
    var renderFrameRate = 50;
    // const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    //const canvas = document.createElement('canvas');
    document.body.appendChild(canvas);
    var ctx = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    if (!canvas)
        return;
    if (!ctx)
        return;
    ctx.imageSmoothingEnabled = true;
    ctx.imageSmoothingQuality = 'high';
    var sim = new Simulation(width, height);
    var imageData = imageCtx.getImageData(0, 0, width, height);
    setInterval(function () {
        sim.Update(imageData);
    }, 1000 / updateFrameRate);
    setInterval(function () {
        sim.Draw(ctx);
    }, 1000 / renderFrameRate);
}
function bootstrapper() {
    var width = window.innerWidth;
    var height = window.innerHeight;
    var imageCanvas = document.createElement('canvas');
    // document.body.appendChild(imageCanvas);
    imageCanvas.width = width;
    imageCanvas.height = height;
    var ctx = imageCanvas.getContext('2d');
    if (!ctx)
        return;
    // image
    var image = new window.Image();
    if (!image)
        return;
    image.crossOrigin = 'Anonymous';
    image.onload = function (e) {
        ctx.drawImage(image, 0, 0, width, height);
        createDrawCanvas(ctx, width, height);
    };
    var images = ['../assets/images/tessy.jpg', '../assets/images/zx10r-2004.jpg', '../assets/images/zx10r-logo.jpg', '../assets/images/fb.jpg', '../assets/images/yoji.jpg', '../assets/images/smiley.jpg'];
    image.src = images[GetRandomInt(0, images.length)];
}
bootstrapper();
